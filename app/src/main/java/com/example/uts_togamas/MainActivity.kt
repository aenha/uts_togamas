package com.example.uts_togamas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SimpleAdapter
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "katalog"
    val F_ID = "id"
    val F_JENIS = "jenis"
    val F_NAMA = "nama"
    val F_DESKRIPSI = "deskripsi"
    val F_TANGGAL = "tanggal"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alKatalog : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        alKatalog = ArrayList()
    }

    override fun onClick(v: View?) {
        when(v?.id){

        }
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            showData()
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alKatalog.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_JENIS,doc.get(F_JENIS).toString())
                hm.set(F_NAMA,doc.get(F_NAMA).toString())
                hm.set(F_DESKRIPSI,doc.get(F_DESKRIPSI).toString())
                hm.set(F_TANGGAL,doc.get(F_TANGGAL).toString())
                alKatalog.add(hm)
            }
            adapter = SimpleAdapter(this,alKatalog,R.layout.row_katalog,
                arrayOf(F_JENIS,F_NAMA,F_DESKRIPSI,F_TANGGAL),
                intArrayOf(R.id.txJenis, R.id.txNama, R.id.txTanggal, R.id.txDeskripsi))
            lsView.adapter = adapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemAbout ->{
                var intent = Intent(this,AboutActivity::class.java)
                startActivity(intent)
            }
            R.id.itemLogoutAdmin ->{
                var intent = Intent(this,ActivityLogin::class.java)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }
}