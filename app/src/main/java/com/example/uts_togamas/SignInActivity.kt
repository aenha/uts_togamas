package com.example.uts_togamas

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "katalog"
    val F_ID = "id"
    val F_JENIS = "jenis"
    val F_NAMA = "nama"
    val F_DESKRIPSI = "deskripsi"
    val F_TANGGAL = "tanggal"
    var docId = ""
    var fbAuth = FirebaseAuth.getInstance()
    lateinit var db : FirebaseFirestore
    lateinit var alKatalog : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        alKatalog = ArrayList()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            showData()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btnInsert -> {
                val hm = HashMap<String,Any>()
                hm.set(F_ID,edId.text.toString())
                hm.set(F_JENIS,edJenis.text.toString())
                hm.set(F_NAMA,edNama.text.toString())
                hm.set(F_DESKRIPSI,edDeskripsi.text.toString())
                hm.set(F_TANGGAL,edTanggal.text.toString())
                db.collection(COLLECTION).document(edId.text.toString()).set(hm).
                addOnSuccessListener {
                    Toast.makeText(this, "Data Successfully Added", Toast.LENGTH_SHORT)
                        .show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data Unsuccessfully Added : ${e.message}", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            R.id.btnUpdate -> {
                val hm = HashMap<String,Any>()
                hm.set(F_ID,docId)
                hm.set(F_JENIS,edJenis.text.toString())
                hm.set(F_NAMA,edNama.text.toString())
                hm.set(F_DESKRIPSI,edDeskripsi.text.toString())
                hm.set(F_TANGGAL,edTanggal.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Successfully Updated", Toast.LENGTH_SHORT)
                            .show() }
                    .addOnFailureListener {e ->
                        Toast.makeText(this, "Data Unsuccessfully Updated : ${e.message}", Toast.LENGTH_SHORT)
                            .show() }
            }
            R.id.btnDelete -> {
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                        results ->
                    for (doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this,"Data Successfully Deleted",
                                    Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(this,"Data Unsuccessfully Deleted ${e.message}",
                                    Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this,"Can't Get Data's References ${e.message}",
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alKatalog.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edJenis.setText(hm.get(F_JENIS).toString())
        edNama.setText(hm.get(F_NAMA).toString())
        edDeskripsi.setText(hm.get(F_DESKRIPSI).toString())
        edTanggal.setText(hm.get(F_TANGGAL).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alKatalog.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_JENIS,doc.get(F_JENIS).toString())
                hm.set(F_NAMA,doc.get(F_NAMA).toString())
                hm.set(F_DESKRIPSI,doc.get(F_DESKRIPSI).toString())
                hm.set(F_TANGGAL,doc.get(F_TANGGAL).toString())
                alKatalog.add(hm)
            }
            adapter = SimpleAdapter(this,alKatalog,R.layout.row_katalog,
                arrayOf(F_JENIS,F_NAMA,F_DESKRIPSI,F_TANGGAL),
                intArrayOf(R.id.txJenis, R.id.txNama, R.id.txTanggal, R.id.txDeskripsi))
            lsData.adapter = adapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option2,menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemLogoutAdmin ->{
                fbAuth.signOut()
                val intent = Intent (this,MainActivity::class.java)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }
}